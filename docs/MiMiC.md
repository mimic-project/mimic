---
project: MiMiC
summary: MiMiC: A Framework for Multiscale Modeling in Computational Chemistry
project_website: https://mimic-project.org/
author: The MiMiC Contributors
version: 0.2.0
year: 2015-2022
doc_license: by-sa
src_dir: ../src
output_dir: ./public
graph: true
display: public
         protected
         private
source: true
search: true
---
