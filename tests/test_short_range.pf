!
!    MiMiC: A Framework for Multiscale Modeling in Computational Chemistry
!    Copyright (C) 2015-2022  The MiMiC Contributors (see CONTRIBUTORS file for details).
!
!    This file is part of MiMiC.
!
!    MiMiC is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as
!    published by the Free Software Foundation, either version 3 of
!    the License, or (at your option) any later version.
!
!    MiMiC is distributed in the hope that it will be useful, but
!    WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!

module test_short_range

    use pFUnit
    use mimic_precision
    use mimic_types
    use mimic_particles
    use mimic_fragments
    use mimic_properties
    use mimic_cells
    use mimic_field_grids
    use mimic_short_range

    implicit none

    @TestCase
    type, extends(TestCase) :: test_sr_type
        real(dp) :: energy = 0.0_dp
        real(dp) :: t1, t2
        real(dp), dimension(3) :: shift
        real(dp), dimension(:, :), allocatable :: tau, tau_nuc
        real(dp), dimension(:, :), allocatable :: forces, forces_nuc
        real(dp), dimension(:, :, :), allocatable :: ref_pot
        real(dp), dimension(:, :), allocatable :: forces_ref, forces_nuc_ref
        real(dp), dimension(:, :), allocatable :: forces_atoms_ref, forces_total_ref
        real(dp), dimension(:), allocatable :: q_mm, q_qm
        real(dp), dimension(:), allocatable :: charge
        real(dp), dimension(:, :, :), allocatable :: rho
        real(dp), dimension(:, :, :), allocatable :: extf
        real(dp), dimension(:), allocatable :: r_n
        type(quantum_fragment_type) :: quantum_fragment
        type(fragment_type) :: current_fragment
        type(density_type) :: density
        type(potential_type) :: potential
        type(nucleus_type), dimension(:), allocatable :: nuclei
        type(atom_type), dimension(:), allocatable :: atoms
        type(multipoles_type), allocatable :: multipoles
        type(cell_type) :: cell
        integer :: num_atoms, num_nuclei
        integer, dimension(3) :: number_points
        real(dp), dimension(3,3) :: lattice_vec
        real(dp), dimension(3) :: origin_coor
        real(dp), pointer :: field(:, :, :)
        real(dp) :: mass
        real(dp) :: abs_diff
        real(dp) :: da,db,dc
        real(dp) :: energy_ref
        real(dp) :: energy_atoms_ref
        real(dp) :: tolerance
    contains
        procedure :: setUp
        procedure :: tearDown
    end type test_sr_type

contains

subroutine setUp(this)

    class(test_sr_type), intent(inout) :: this
    integer :: n_atom
    this%tolerance = 1.0e-14_dp

    this%num_atoms = 5
    this%num_nuclei = 3
    this%number_points(:) = 2

    allocate(this%forces_ref(3, this%num_atoms))
    allocate(this%forces_nuc_ref(3, this%num_nuclei))
    allocate(this%forces_atoms_ref(3, this%num_atoms))
    allocate(this%forces_total_ref(3, this%num_atoms))

    allocate(this%ref_pot(this%number_points(1) + 1, &
                          this%number_points(2) + 1, &
                          this%number_points(3) + 1))

    this%ref_pot(:,:,:) = 0.0_dp

    this%ref_pot(1, 1, 1) = -0.5755680331572270_dp
    this%ref_pot(1, 1, 2) = -0.6452885780979548_dp
    this%ref_pot(1, 2, 1) = -0.6359172510851052_dp
    this%ref_pot(1, 2, 2) = -0.7945326899792222_dp
    this%ref_pot(2, 1, 1) = -0.6748010052694792_dp
    this%ref_pot(2, 1, 2) = -0.8037155005455702_dp
    this%ref_pot(2, 2, 1) = -0.7788019232744658_dp
    this%ref_pot(2, 2, 2) = -1.2288910651300875_dp

    this%forces_ref(:, 1) = [-0.1066753807319338_dp,  0.3095966320567635_dp,  0.1335682235264074_dp]
    this%forces_ref(:, 2) = [-0.5994658639210609_dp, -0.1593238245672671_dp, -0.3885706067192896_dp]
    this%forces_ref(:, 3) = [-0.0489905487315048_dp, -0.1046850589302512_dp, -0.0774409258742298_dp]
    this%forces_ref(:, 4) = [ 0.0016584761132813_dp, -0.0535033269497453_dp, -0.0161183496951742_dp]
    this%forces_ref(:, 5) = [-0.6756022648548269_dp,  0.2090254003079543_dp, -0.2270321324251027_dp]

    this%energy_ref = 9.903601070813108_dp

    this%forces_atoms_ref(:, 1) = [-0.6170449110298734_dp,  0.4777629837663552_dp,  0.5467962122946170_dp]
    this%forces_atoms_ref(:, 2) = [ 0.0348847685587972_dp,  0.2914076806488268_dp,  0.8225073656948960_dp]
    this%forces_atoms_ref(:, 3) = [ 0.2446106502712133_dp,  0.1073304762908418_dp,  0.0966693862997445_dp]
    this%forces_atoms_ref(:, 4) = [ 0.2738653743559572_dp, -0.1793758468496553_dp, -0.2987424290772808_dp]
    this%forces_atoms_ref(:, 5) = [-0.3529071414644218_dp, -0.0469063379365392_dp,  0.4930339896908679_dp]

    this%forces_nuc_ref(:, 1) = [-0.1369649461950442_dp, -0.1208527962471140_dp, -0.1047406462991838_dp]
    this%forces_nuc_ref(:, 2) = [-0.3283384929275035_dp, -1.4145584868594572_dp, -1.3277921983590939_dp]
    this%forces_nuc_ref(:, 3) = [ 0.8818946984308752_dp,  0.8851923271867416_dp, -0.2277316802445667_dp]

    this%forces_total_ref = this%forces_atoms_ref + this%forces_ref

    this%lattice_vec(1:3, 1) = [2.0000000000000000_dp, 0.0000000000000000_dp, 0.0000000000000000_dp]
    this%lattice_vec(1:3, 2) = [0.0000000000000000_dp, 4.0000000000000000_dp, 0.0000000000000000_dp]
    this%lattice_vec(1:3, 3) = [0.0000000000000000_dp, 0.0000000000000000_dp, 3.0000000000000000_dp]
    allocate(this%rho(3, 3, 3))
    this%rho(:,:,:) = 0.0_dp
    this%rho(1, 1, 1) = 0.2500000000000000_dp
    this%rho(1, 1, 2) = 0.2500000000000000_dp
    this%rho(1, 2, 1) = 0.5000000000000000_dp
    this%rho(1, 2, 2) = 0.5000000000000000_dp
    this%rho(2, 1, 1) = 0.0010000000000000_dp
    this%rho(2, 1, 2) = 0.2000000000000000_dp
    this%rho(2, 2, 1) = 0.3000000000000000_dp
    this%rho(2, 2, 2) = 0.2000000000000000_dp

    allocate(this%tau(3, this%num_atoms))
    allocate(this%tau_nuc(3, this%num_nuclei))
    this%tau(1:3, 1) = [0.5000000000000000_dp, 0.5000000000000000_dp, 0.5000000000000000_dp]
    this%tau(1:3, 2) = [2.0000000000000000_dp, 2.0000000000000000_dp, 2.0000000000000000_dp]
    this%tau(1:3, 3) = [-0.5000000000000000_dp, -0.5000000000000000_dp, -0.5000000000000000_dp]
    this%tau(1:3, 4) = [0.2500000000000000_dp, 0.5000000000000000_dp, 0.7500000000000000_dp]
    this%tau(1:3, 5) = [1.5000000000000000_dp, 1.5000000000000000_dp, 1.5000000000000000_dp]

    this%tau_nuc(1:3, 1) = [0.0000000000000000_dp, 0.0000000000000000_dp, 0.0000000000000000_dp]
    this%tau_nuc(1:3, 2) = [1.0000000000000000_dp, 0.0000000000000000_dp, 0.0000000000000000_dp]
    this%tau_nuc(1:3, 3) = [2.5000000000000000_dp, 2.5000000000000000_dp, 1.5000000000000000_dp]

    allocate(this%forces(3, this%num_atoms))
    allocate(this%forces_nuc(3, this%num_nuclei))

    allocate(this%q_mm(this%num_atoms))
    allocate(this%q_qm(this%num_nuclei))
    allocate(this%r_n(this%num_atoms))
    allocate(this%charge(this%num_atoms))

    allocate(this%extf(this%number_points(1) + 1, &
                       this%number_points(2) + 1, &
                       this%number_points(3) + 1))
    allocate(this%nuclei(this%num_nuclei))
    allocate(this%atoms(this%num_atoms))
    allocate(this%multipoles)

    this%charge(1) = 1.2000000000000000_dp
    this%charge(2) = 1.5000000000000000_dp
    this%charge(3) = 2.0000000000000000_dp
    this%charge(4) = 0.8000000000000000_dp
    this%charge(5) = 0.5000000000000000_dp

    this%forces(:,:) = 0
    this%forces_nuc(:,:) = 0

    this%q_mm(1) = 0.5000000000000000_dp
    this%q_mm(2) = 1.0000000000000000_dp
    this%q_mm(3) = -0.3000000000000000_dp
    this%q_mm(4) = -0.1000000000000000_dp
    this%q_mm(5) = 0.4000000000000000_dp

    this%q_qm(1) = 1.0000000000000000_dp
    this%q_qm(2) = 8.0000000000000000_dp
    this%q_qm(3) = 4.0000000000000000_dp

    this%origin_coor(:) = 0.0000000000000000_dp
    this%shift(:) = 0.0000000000000000_dp

    this%origin_coor = this%origin_coor + this%shift

    call this%cell%init(id=0, &
                   num_points=this%number_points, &
                   num_points_r=this%number_points + 1, &
                   origin=this%origin_coor, &
                   lattice=this%lattice_vec)
    call this%density%init(id=0, field=this%rho)
    this%extf(:,:,:) = 0
    call this%potential%init(id=0, field=this%extf)

    do n_atom = 1, this%num_nuclei
        call this%nuclei(n_atom)%init(id=n_atom, species_id=1, &
                                      atom_id=n_atom, &
                                      charge=this%q_qm(n_atom), &
                                      coordinate=this%tau_nuc(1:3, n_atom), &
                                      force=this%forces_nuc(1:3, n_atom), &
                                      overlapped=.false.)
    enddo

    do n_atom = 1, this%num_atoms
        call this%atoms(n_atom)%init(n_atom, 1, n_atom, this%q_mm(n_atom), &
                                     this%charge(n_atom), this%tau(1:3, n_atom), &
                                     this%forces(1:3, n_atom), this%multipoles, .false.)
    enddo

    call this%quantum_fragment%init(id=1, nuclei=this%nuclei, cell=this%cell, &
                                    density=this%density, potential=this%potential)

end subroutine setUp

subroutine tearDown(this)

    class(test_sr_type), intent(inout) :: this

    deallocate(this%forces_ref)
    deallocate(this%forces_nuc_ref)
    deallocate(this%forces_atoms_ref)
    deallocate(this%forces_total_ref)
    deallocate(this%ref_pot)
    deallocate(this%rho)
    deallocate(this%tau)
    deallocate(this%tau_nuc)
    deallocate(this%forces)
    deallocate(this%forces_nuc)
    deallocate(this%q_mm)
    deallocate(this%q_qm)
    deallocate(this%r_n)
    deallocate(this%charge)
    deallocate(this%extf)
    deallocate(this%nuclei)
    deallocate(this%atoms)
    deallocate(this%multipoles)

end subroutine tearDown

@Test
subroutine testGrid(this)

    class(test_sr_type), intent(inout) :: this

    integer :: n_atom

    call compute_sr_forces_electrons(this%atoms, &
                                this%quantum_fragment%density%field, &
                                this%quantum_fragment%cell, &
                                1, this%cell%num_points(1), &
                                1, size(this%atoms))

    this%quantum_fragment%potential%field(:,:,:) = 0.0_dp
    call compute_sr_potential_electrons(this%atoms, &
                                this%quantum_fragment%potential%field, &
                                this%quantum_fragment%cell, &
                                1, this%cell%num_points(1), &
                                1, size(this%atoms))

    do n_atom = 1, this%num_atoms
        @assertEqual(this%forces_ref(:, n_atom), this%forces(:, n_atom), this%tolerance)
    enddo

    @assertEqual(this%ref_pot, this%extf, this%tolerance)

end subroutine testGrid

@Test
subroutine testGridDist(this)

    class(test_sr_type), intent(inout) :: this
    real(dp), dimension(size(this%extf, 1), size(this%extf, 2), size(this%extf, 3)) :: temp_extf

    integer :: n_atom
    call compute_sr_forces_electrons(this%atoms, &
                                this%quantum_fragment%density%field, &
                                this%quantum_fragment%cell, &
                                1, this%cell%num_points(1), &
                                1, 2)
    call compute_sr_potential_electrons(this%atoms, &
                                this%quantum_fragment%potential%field, &
                                this%quantum_fragment%cell, &
                                1, this%cell%num_points(1), &
                                1, 2)

    temp_extf = this%extf

    this%extf = 0.0_dp

    call compute_sr_forces_electrons(this%atoms, &
                                this%quantum_fragment%density%field, &
                                this%quantum_fragment%cell, &
                                1, this%cell%num_points(1), &
                                3, 5)
    call compute_sr_potential_electrons(this%atoms, &
                                this%quantum_fragment%potential%field, &
                                this%quantum_fragment%cell, &
                                1, this%cell%num_points(1), &
                                3, 5)

    this%extf = this%extf + temp_extf

    do n_atom = 1, this%num_atoms
        @assertEqual(this%forces_ref(:, n_atom), this%forces(:, n_atom), this%tolerance)
    enddo

    @assertEqual(this%ref_pot, this%extf, this%tolerance)

end subroutine testGridDist

@Test
subroutine testAtoms(this)

    class(test_sr_type), intent(inout) :: this

    integer :: n_atom

    call compute_sr_energy_ions(this%atoms, this%quantum_fragment%nuclei, &
                                this%energy, 1, size(this%atoms))
    call compute_sr_forces_ions(this%atoms, this%quantum_fragment%nuclei, &
                                1, size(this%atoms))

    do n_atom = 1, this%num_atoms
        @assertEqual(this%forces_atoms_ref(:, n_atom), this%forces(:, n_atom), this%tolerance)
    enddo

    do n_atom = 1, this%num_nuclei
        @assertEqual(this%forces_nuc_ref(:, n_atom), this%forces_nuc(:, n_atom), this%tolerance)
    enddo

    @assertEqual(this%energy_ref, this%energy, this%tolerance * 5)

end subroutine testAtoms

@Test
subroutine testAtomsDist(this)

    class(test_sr_type), intent(inout) :: this

    integer :: n_atom
    real(dp) :: temp_energy

    call compute_sr_energy_ions(this%atoms, this%quantum_fragment%nuclei, &
                                temp_energy, 1, 2)
    call compute_sr_energy_ions(this%atoms, this%quantum_fragment%nuclei, &
                                this%energy, 3, 5)
    this%energy = this%energy + temp_energy

    call compute_sr_forces_ions(this%atoms, this%quantum_fragment%nuclei, 1, 2)
    call compute_sr_forces_ions(this%atoms, this%quantum_fragment%nuclei, 3, 5)

    do n_atom = 1, this%num_atoms
        @assertEqual(this%forces_atoms_ref(:, n_atom), this%forces(:, n_atom), this%tolerance)
    enddo

    do n_atom = 1, this%num_nuclei
        @assertEqual(this%forces_nuc_ref(:, n_atom), this%forces_nuc(:, n_atom), this%tolerance)
    enddo

    @assertEqual(this%energy_ref, this%energy, this%tolerance * 5)

end subroutine testAtomsDist

@Test
subroutine testAll(this)

    class(test_sr_type), intent(inout) :: this

    integer :: n_atom

    call compute_sr_forces_ions(this%atoms, this%quantum_fragment%nuclei, 1, size(this%atoms))

    this%extf = 0.0_dp

    call compute_sr_forces_electrons(this%atoms, &
                                    this%quantum_fragment%density%field, &
                                    this%quantum_fragment%cell, &
                                    1, this%cell%num_points(1), &
                                    1, size(this%atoms))

    do n_atom = 1, this%num_atoms
        @assertEqual(this%forces_total_ref(:, n_atom), this%forces(:, n_atom), this%tolerance)
    enddo


end subroutine testAll

end module test_short_range
