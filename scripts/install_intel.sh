#!/bin/bash

dnf install -y -q intel-oneapi-compiler-dpcpp-cpp-2022.1.0 \
                  intel-oneapi-compiler-dpcpp-cpp-and-cpp-classic-2022.1.0 \
                  intel-oneapi-compiler-fortran-2022.1.0 \
                  intel-oneapi-mpi-2021.7.0 \
                  intel-oneapi-mpi-devel-2021.7.0
