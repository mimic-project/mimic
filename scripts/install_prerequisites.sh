#!/bin/bash

dnf install -y -q git make cmake \
                  gcc g++ gfortran \
                  mpich mpich-devel \
                  openmpi openmpi-devel \
                  findutils procps-ng coreutils m4
