!
!    MiMiC: A Framework for Multiscale Modeling in Computational Chemistry
!    Copyright (C) 2015-2022  The MiMiC Contributors (see CONTRIBUTORS file for details).
!
!    This file is part of MiMiC.
!
!    MiMiC is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as
!    published by the Free Software Foundation, either version 3 of
!    the License, or (at your option) any later version.
!
!    MiMiC is distributed in the hope that it will be useful, but
!    WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!

!> author: Jógvan Magnus Haugaard Olsen
!> defines single-, double-, and quadruple-precision parameters
module mimic_precision

    ! Fortran 2008
    use, intrinsic :: iso_fortran_env, only: real32, real64, real128, &
                                             int32, int64
    !> single-precision parameter
    integer, parameter, public :: sp = real32
    !> double-precision parameter
    integer, parameter, public :: dp = real64
    !> quadruple-precision parameter
    integer, parameter, public :: qp = real128
    !> 32-bit integer
    integer, parameter, public :: i32 = int32
    !> 64-bit integer
    integer, parameter, public :: i64 = int64

end module mimic_precision
